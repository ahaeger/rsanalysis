% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/node_measures.R
\name{node_measures}
\alias{node_measures}
\title{Get measures grouped by node names.}
\usage{
node_measures(datalist, subjects, name, measure)
}
\arguments{
\item{datalist}{The data to work on.}

\item{subjects}{A table identifying subjects and groups.}

\item{name}{The varibale to group by.}

\item{measure}{The measure to find data for.}
}
\value{
A summary table.
}
\description{
Get measures grouped by node names.
}
