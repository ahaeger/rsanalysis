# The rsAnalysis package

This package is a complement to the `rsfmri` package available 
[here](https://gitlab.com/choh/rsfmri) and provides higher level tools for 
working with resting-state fMRI data. 
Given that the functions provided by the `rsfrmi` are more generalised and the 
function provided here are more specialised, the packages have been split. 

The available tools include a pre-processing pipeline as well as tools 
for graph theoretical analysis of resting-state fMRI data. 

This package has been created in the research group Imaging in
Neurodegenerative Diseases at the University Hospital Aachen, Germany.


## Installation
You need to install the [`rsfmri` package](https://gitlab.com/choh/rsfmri) 
first, afterwards calling `devtools::install_gitlab("choh/rsanalysis")` should
be sufficient.

## Licence
The package is available under the GNU GPL v3.

