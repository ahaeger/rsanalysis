#' Generate confounders for _regressing out_ of a dataset.
#'
#' @param motion_file A file containing motion confounders.
#' @param ... A list of files already present to include into the correction.
#' @param first_deriv Boolean indicating whether to calculate the first
#' derivative.
#' @param second_deriv Boolean indicating whether to calculate the second
#' derivative.
#' @param square Boolean indicating whether to calculate the second
#' derivative.
#' @param intercept Boolean indicating whether to include a constant for the
#' intercept.
#'
#' @return A data.frame containing the confounders.
#' @export
noise_file <- function(motion_file, ..., first_deriv = TRUE,
                       second_deriv = FALSE, square = TRUE,
                       intercept = TRUE) {
    motion_regs <- readr::read_table(motion_file, col_names = FALSE)
    vars <- ncol(motion_regs)
    colnames(motion_regs) <- paste0("motion", sprintf("%02d", 1:vars))

    if (first_deriv || second_deriv || square) {
        motion_add <- list()
        if (first_deriv) {
            fdv <- apply(motion_regs, 2, cdiff)
            colnames(fdv) <- paste0("motion_1deriv", sprintf("%02d", 1:vars))
            motion_add[[length(motion_add) + 1]] <- fdv
        }
        if (second_deriv) {
            # don't rely on fdv being available here
            sdv <- apply(motion_regs, 2, function(x) cdiff(cdiff(x)))
            colnames(sdv) <- paste0("motion_2deriv", sprintf("%02d", 1:vars))
            motion_add[[length(motion_add) + 1]] <- sdv
        }
        if (square) {
            motion_sq <- motion_regs ^ 2
            colnames(motion_sq) <- paste0("motion_sq", sprintf("%02d", 1:vars))
            motion_add[[length(motion_add) + 1]] <- motion_sq
        }
        motion_add <- Reduce(function(x, y) cbind(x, y), motion_add)
        motion_regs <- cbind(motion_regs, motion_add)
    }

    additional_files <- list(...)

    if (length(additional_files) > 0) {
        add_data <- lapply(additional_files,
                           readr::read_table, col_names = FALSE)
        row_num <- lapply(add_data, nrow) |> unlist()
        if (!all(row_num == nrow(motion_regs))) {
            stop("Additional files must have the same length a the motion
                  time course.")
        }
        add_data <- Reduce(function(x, y) cbind(x, y), add_data)
        add_vars <- ncol(add_data)
        colnames(add_data) <- paste0("add", sprintf("%02d", 1:add_vars))
        motion_regs <- cbind(motion_regs, add_data)
    }

    if (intercept) {
        motion_regs[["intercept"]] <- 1
    }

    motion_regs
}
