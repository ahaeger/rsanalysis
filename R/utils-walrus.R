#' Pipe operator
#'
#' See \code{rlang::\link[rlang:nse-force]{:=}} for details.
#'
#' @name :=
#' @rdname walrus
#' @keywords internal
#' @export
#' @importFrom rlang :=
NULL
